import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._

import play.api.Logger

import persistence.KingFisherDAO
import persistence.DAOFactory

import model.User
import model.Reviewable
import model.Discriminator
import model.Rating


@RunWith(classOf[JUnitRunner])
class ModelSpec extends Specification {

  "Application" should {

    "send 404 on a bad request" in new WithApplication {

      var usr = new User(name = "TestUser", email = "joselor@gmail.com", password = "123456")
      var rev = new Reviewable(id = -1, name= "Applebees", description =  "Applebees restaurant Mall of America" )  
      var disc1 = new Discriminator (id = -1, name = "Menu", description="Kids menu" )
      var disc2 = new Discriminator (id = -1, name = "Parking", description="Parking facility" )
      
     
      val dao = DAOFactory.createKingFisherDAO
      
      //dao.addUser(usr)
      //dao.addReviewable(rev)
     // rev = dao.getReviewableById(31)
     // dao.addDiscriminator(disc1,rev)
     // dao.addDiscriminator(disc2,rev)
      
      usr = dao.getUserByName("TestUser")
      var allDisc = dao.getDiscriminatorsForReviewable(31)

      dao.addRatingForNode(   allDisc(0), new  Rating (rating = 3.6, user = usr)   )
      
    }
  }
}