
package model

import java.net.URL

class CommonProperties {
  
  val phone: String = ""
  val website: URL = null
  val address : Address = null
  
}