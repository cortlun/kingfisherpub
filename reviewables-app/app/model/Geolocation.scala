package model

class Geolocation {
  
  var countryCode: String = _
  var countryName: String = _
  var regionName: String = _
  var cityName: String = _
  var cipCode: String = _
  var latitude: Double = _
  var longitude: Double = _

}