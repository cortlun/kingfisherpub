

package model

import scala.collection.mutable.MutableList


class Node( val id:Long = -1,
  val createDate: Long = System.currentTimeMillis / 1000,
  val properties: MutableList[Property] = new MutableList(),
  val status:Int = 0 ){
  
}