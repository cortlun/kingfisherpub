
package model
import scala.collection.mutable.MutableList

class Discriminator(id:Long = -1, name:String = "", description:String = "", 
    ratings : MutableList[Rating] = new MutableList[Rating],
    rating: Double = 0.0, numRatings:Long = 0, numReviews:Long = 0, 
    val parentDiscriminators : MutableList[Reviewable] = new MutableList[Reviewable], 
    val childDiscriminators : MutableList[Discriminator] = new MutableList[Discriminator], 
        parentReviewable : Reviewable = null) extends Reviewable(id = id, name = name, 
            description = description, rating = rating, 
            discriminators = childDiscriminators, numRatings = numRatings, numReviews = numReviews) {
}