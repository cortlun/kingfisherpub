package model
import scala.collection.mutable.MutableList

case class  Reviewable(override val id : Long = -1,
    val name : String = "", 
    val description : String = "", 
    val discriminators : MutableList[Discriminator] = new MutableList[Discriminator],
    val rating : Double = 0.0,
    val numRatings : Long = 0, 
    val numReviews : Long = 0,  
    val ratings : MutableList[Rating] = new MutableList[Rating], 
    val thumbnail : String = "", 
    val relatedReviewables : MutableList[RelatedReviewable] = new MutableList[RelatedReviewable], 
    val labels : MutableList[String] = new MutableList[String]) extends Node (id) {
  
}