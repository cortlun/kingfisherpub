package controllers

import play.api._
import scala.collection.mutable.MutableList
import views._
import play.api.data.Forms._
import play.api.data._
import play.api.mvc._
import play.api.db._
import play.api.Play.current
import model._
import persistence._
import play.mvc.Http.Context
import play.api.Logger
import java.text.SimpleDateFormat
import java.util.Calendar
import java.net._

import persistence.DAOFactory

object Application extends Controller {
  val searchForm = Form(
    single(
         "name" -> text
         )
  )
  
  val discriminatorForm = Form(
    mapping(
    "name" -> nonEmptyText,
    "description" -> text,
    "nodeId" -> longNumber
    )(DiscriminatorData.apply)(DiscriminatorData.unapply)  
  )
  
  val commentForm = Form(
     mapping(
       "reviewId" -> longNumber,
       "ratingId" -> longNumber,
       "comment" -> nonEmptyText,
       "reviewableId" -> longNumber
     )(CommentData.apply)(CommentData.unapply)
  )
  
  val invokeCommentForm = Form(
    mapping(
        "reviewableId" -> longNumber,
        "ratingId" -> longNumber
        )(InvokeCommentForm.apply)(InvokeCommentForm.unapply)
  )
  
  val registrationForm = Form(
    mapping(
      "username"  -> nonEmptyText,
      "email" -> nonEmptyText,
      "password" -> nonEmptyText,
      "confirmPassword" -> nonEmptyText
    )(RegistrationData.apply)(RegistrationData.unapply)
  )
  
  val newReviewableForm = Form(
      mapping(
        "name" -> nonEmptyText,
        "description" -> text,
        "thumbnailurl" -> text
      )(ReviewableData.apply)(ReviewableData.unapply)
  )
 
  val loginForm = Form(
    mapping(
       "login" -> nonEmptyText,
       "password" -> nonEmptyText
    )(UserData.apply)(UserData.unapply)
  )
  
  val reviewableForm = Form {
    mapping(
        "name" -> nonEmptyText,
        "description" -> text,
        "thumbnailUrl" -> text
    )(ReviewableData.apply)(ReviewableData.unapply)
  }
  
  val ratingForm = Form {
    mapping(
        "rating" -> text,
        "review" -> text,
        "reviewableId" -> number
    )(RatingData.apply)(RatingData.unapply)
  }
  
  def registerForm() = Action {
    Ok(views.html.register(registrationForm))
  }
  
  def review(ratingId : Long, reviewableId : Long) = Action{
    val dao = DAOFactory.createKingFisherDAO
    val rev = dao.getReviewableById(reviewableId)
    println("Ratings length: " + rev.ratings.length)
    println("Rating id: " + ratingId)
    val rating = rev.ratings.filter(_.id == ratingId)
    Ok(views.html.review(rating.last, rev, invokeCommentForm))
  }
  def registerUser() = Action {implicit request =>
    registrationForm.bindFromRequest.fold(
    error =>{
      BadRequest(views.html.error("Please make sure all necessary fields are complete."))
    },
    registrationForm => {
      val username = registrationForm.username
      val pass = registrationForm.password
      val confirmPass = registrationForm.confirmPassword
      val email = registrationForm.email
      if (! pass.equals(confirmPass)){
        BadRequest(views.html.error("Passwords do not match."))
      } else {
        val user = new User(name = username, email = email, password= pass)
        val dao = DAOFactory.createKingFisherDAO
        dao.addUser(user)
        Ok(views.html.search(searchForm))
      }
    }
    )
  }
  def ratingform(reviewableId : String) = Action {  implicit request =>
    val dao = DAOFactory.createKingFisherDAO
    val rev = dao.getReviewableById(reviewableId.toInt)
    if (request.session.get("connected").isDefined){
      val username = request.session.get("connected").get
      Ok(views.html.addrating(ratingForm, rev, username))
    } else {
      Ok(views.html.login(loginForm))
    }
  }
  
  def invokecommentform() = Action {  implicit request =>
    if (request.session.get("connected").isDefined){
      invokeCommentForm.bindFromRequest.fold(
          error => {
            BadRequest(views.html.error("Error adding comment: " + error))
          },
          form =>{
            val dao = DAOFactory.createKingFisherDAO
            val username = request.session.get("connected").get
            val reviewableId = form.reviewableId
            val ratingId = form.ratingId
            val rev = dao.getReviewableById(reviewableId)
            println("ratingid: " + ratingId)
            println("reviewableId: " + reviewableId)
            val rating = rev.ratings.filter(_.id == ratingId)
            Ok(views.html.addcomment(rev, rating.last, commentForm))
          }
      )
    } else {
      Ok(views.html.login(loginForm))
    }
  }
  
  def addcomment() = Action { implicit request =>
    commentForm.bindFromRequest.fold(
    error => {
      BadRequest(views.html.error("Errors in comment form: " + error))
    },
    commentform => {
      val localhost = InetAddress.getLocalHost
      val ip = localhost.getHostAddress
      val ratingId = commentform.ratingId
      val reviewableId = commentform.reviewableId
      val reviewId = commentform.reviewId
      val username = request.session.get("connected").get
      val commentText = commentform.comment
      val dao = DAOFactory.createKingFisherDAO
      val rev = dao.getReviewableById(reviewableId)
      val user = dao.getUserByName(username)
      val format = new SimpleDateFormat("MMM-dd-yyyy hh:mm:ss")
      val date = format.format(Calendar.getInstance.getTime)
      val comment = new Comment(user, commentText, date)
      dao.addCommentForReview(reviewId, comment, user)
      val rating = rev.ratings.filter(_.id == ratingId)
      Ok(views.html.reviewable(rev))
    }
    )
    
  }
  
  def subrating(nodeId : Long) = Action { implicit request =>
    if (request.session.get("connected").isDefined){
      val username = request.session.get("connected").toString
      Ok(views.html.subrating(nodeId, discriminatorForm))
    } else {
      Ok(views.html.login(loginForm))
    }
  }
  
  def commitsubrating() = Action {implicit request =>
    discriminatorForm.bindFromRequest.fold(
      error => {
        BadRequest(views.html.error("Error validating input form"))
      },
      discriminatorForm => {
        val name = discriminatorForm.name
        val description = discriminatorForm.description
        val nodeId = discriminatorForm.nodeId
        val discriminator = new Discriminator(name = name, description = description)
        val dao = DAOFactory.createKingFisherDAO
        dao.addDiscriminator(discriminator, nodeId)
        Ok(views.html.search(searchForm))
      }
    )
  }
  def addrating() = Action{ implicit request =>
    val url = request.headers("referer")
    println(url)
    ratingForm.bindFromRequest.fold(
      error => {
        BadRequest(views.html.error("Please provide an integer rating between 1 and 5." + error))
      },
      ratingform => {
        val username = request.session.get("connected").get
        println("username: " + username)
        val rating = ratingform.rating.toInt
        println("rating : " + rating)
        val reviewableId = ratingform.reviewableId
        val review = new Review(review=ratingform.review)
        println("review: " + review.review)
        val dao = DAOFactory.createKingFisherDAO
        val user = dao.getUserByName(username)
        val rate = new Rating(rating=rating, review=review, user=user)
        val labels = dao.getLabelsForNode(reviewableId)
        val nodeType = getNodeType(labels)
        dao.addRatingForNode(user, reviewableId, rate)
        println("Navigating to reviewable page.")
        if(nodeType == "Reviewable"){
        Ok(views.html.reviewable(dao.getReviewableById(reviewableId)))
        } else {
          Ok(views.html.discriminator(dao.getDiscriminatorById(reviewableId)))
        }
  }
      )

  }
  
  def search = Action { implicit request =>
    Ok(views.html.search(searchForm))
  }
  
  def searchresults = Action { implicit request =>
    
    searchForm.bindFromRequest.fold(
        error => {
            BadRequest(views.html.search(error))
        },
        search => {
          val dao = DAOFactory.createKingFisherDAO
          val name = search
          val reviewables = dao.searchReviewablesByName(name)
          Ok(views.html.searchresults(reviewables, name))
        }
    )
    
  }
  
  def shownode(id : String) = Action {
    val nodeId = id.toLong
    val dao = DAOFactory.createKingFisherDAO
    val labels = dao.getLabelsForNode(nodeId)
    val nodeType = getNodeType(labels)
    if (nodeType == "Reviewable"){
      val rev = dao.getReviewableById(nodeId)
      Ok(views.html.reviewable(rev))
    }
    else if (nodeType == "Discriminator"){
      val discriminator = dao.getDiscriminatorById(nodeId)
      println("Discriminator name: " + discriminator.name)
      Ok(views.html.discriminator(discriminator))
    } 
    else {
    BadRequest(views.html.error("Node does not exist or is not viewable"))
    }
  }
  
  def getNodeType(labels : MutableList[String]) : String = {
    for (label <- labels){
      if (label.equals("Discriminator")){
        return "Discriminator"
      }
      else if (label.equals("Reviewable")){
        return "Reviewable"
      } 
      else if (label.equals("Review")){
        return "Review"
      }
    }
    return ""
  }
  
  def login() = Action {
    Ok(views.html.login(loginForm))
  }
  
  
  def addreviewable = Action { implicit request =>
    if (request.session.get("connected").isDefined){
      val username = request.session.get("connected").toString
      Ok(views.html.addreviewable(reviewableForm, username))
    } else {
      Ok(views.html.login(loginForm))
    }
  }
  
  def logout = Action {implicit request =>
    Ok(views.html.search(searchForm)).withNewSession
  }
  
  def commitreviewable = Action {implicit request =>
    reviewableForm.bindFromRequest.fold(
      error => {
        BadRequest(views.html.error("Error validating input form"))
      },
      reviewable => {
        val name = reviewable.name
        val description = reviewable.description
        val thumbnailUrl = reviewable.thumbnailUrl
        val rev = new Reviewable(name = name, description = description, thumbnail = thumbnailUrl )
        val dao = DAOFactory.createKingFisherDAO
        dao.addReviewable(rev)
        Ok(views.html.search(searchForm))
      }
    )
    
  }
  
  def validateLogin() = Action { implicit request =>
    val url = request.headers("referer")
    loginForm.bindFromRequest.fold(
      error => {
        BadRequest(views.html.error("Error validating input form"))
      },
      login => {
        val name = login.login
        val inputPassword = login.password
        val dao = DAOFactory.createKingFisherDAO
        val user = dao.getUserByName(name)
        if (user.password == inputPassword){
          Logger.info("Login success")
          Redirect(url).withSession("connected" -> name)
        } else {
          Logger.info("Login failure")
          BadRequest(views.html.error("Login failed.  Invalid login or password"))
        }
      }
    )
  }  
    def index = Action {
  Ok(views.html.search(searchForm))
  }
}