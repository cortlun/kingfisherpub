package persistence

import model._
import scala.collection.mutable.MutableList
import java.sql.Connection
import java.sql.Statement
import java.sql.SQLException
import java.sql.ResultSet
import java.math.MathContext

import play.api.Logger

class Neo4jDAO extends KingFisherDAO {

  var ds = Neo4jDatasource.ds

  def getReviewableById(id: Long): Reviewable = {
    val con = ds.getConnection
    val reviewable = getReviewableById(id, con)
    con.close
    return reviewable
  }
  def addCommentForReview(reviewId : Long, comment : Comment, user : User){
    val con = ds.getConnection
    val statement = con.prepareStatement("MATCH (r), (u) WHERE id(r)={1} and id(u)={2} CREATE (u)-[:CREATED]->(c:Comment {comment : {3}, createddate : {4}})-[:COMMENT_FOR]->(r)")
    statement.setLong(1, reviewId)
    statement.setLong(2, comment.user.id)
    statement.setString(3, comment.comment)
    statement.setString(4, comment.createdDate)
    statement.executeQuery
    statement.close
    con.close
  }
  
  def getLabelsForNode(id: Long): MutableList[String] = {
    val con = ds.getConnection
    val labels = getLabelsForNode(id, con)
    con.close
    labels
  }
  
  def getDiscriminatorById(id : Long) : Discriminator = {
    val con  = ds.getConnection
    val discriminator = getDiscriminatorById(id, con)
    con.close
    discriminator
  }
  
  private def getDiscriminatorById(id: Long, con : Connection) : Discriminator = {
    val statement = con.prepareStatement("MATCH (n:Discriminator) WHERE id(n) = {1} return n.name, n.description")
    statement.setLong(1, id)
    val rs = statement.executeQuery
    rs.next
    val name = rs.getString("n.name")
    val description = rs.getString("n.description")
    val ratings = getRatingsForReviewable(id, con)
    val rating = getAverageRating(ratings)
    val discriminators = getDiscriminatorsForReviewable(id, con)
    val parentDiscriminators = getParentDiscriminatorsForDiscriminator(id, con)
    val parentReviewable = parentDiscriminators.last
    return new Discriminator(id = id, name = name, rating = rating, ratings = ratings, childDiscriminators = discriminators, parentDiscriminators = parentDiscriminators, parentReviewable = parentReviewable, description = description)
  }
  
  private def getParentDiscriminatorsForDiscriminator(id : Long, con : Connection) : MutableList[Reviewable]= {
    val statement = con.prepareStatement("MATCH p=(n)-[*0..5]->(r) Where id(n) = {1} with nodes(p) as nodes unwind nodes as node return distinct id(node)")
    statement.setLong(1, id)
    val rs = statement.executeQuery
    val parentDiscriminators = new MutableList[Reviewable]
    while (rs.next){
      val id = rs.getLong("id(node)")
      val labels = getLabelsForNode(id, con)
      val nodeType = getNodeType(labels)
      if (nodeType == "Discriminator"){
        val ratings = getRatingsForReviewable(id, con)
        val rating = getAverageRating(ratings)
        val dstat = con.prepareStatement("MATCH (n) where id(n) = {1} return n.name, n.description")
        dstat.setLong(1, id)
        val drs = dstat.executeQuery
        drs.next
        val name = drs.getString("n.name")
        val description = drs.getString("n.description")
        val discriminator = new Discriminator(id = id, name = name, rating = rating)
        parentDiscriminators += discriminator
      } else if (nodeType == "Reviewable"){
        val ratings = getRatingsForReviewable(id, con)
        val rating = getAverageRating(ratings)
        val rstat = con.prepareStatement("MATCH (n) where id(n) = {1} return n.name, n.description")
        rstat.setLong(1, id)
        val rrs = rstat.executeQuery
        rrs.next
        val name = rrs.getString("n.name")
        val description = rrs.getString("n.description")
        val reviewable = new Reviewable(id = id, name = name, rating = rating)
        parentDiscriminators += reviewable
        return parentDiscriminators
    }
    }
      return parentDiscriminators
  }
  def getAverageRating(ratings: MutableList[Rating]): Double = {
    val avg = ratings.foldLeft(0.0)(_ + _.rating) / ratings.length
    if (avg.isNaN){
      return 0.0
    } else {
      return BigDecimal(avg).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
    }
  }

  def getReviewForRating(ratingId: Long): Review = {
    val con = ds.getConnection
    val review = getReviewForRating(ratingId, con)
    con.close
    return review
  }
  
  def getNodeType(labels : MutableList[String]) : String ={
    for (label <- labels){
      println(label)
      if (label.equals("Discriminator") || label.equals("DISCRIMINATOR")){
          return "Discriminator"
      }
      else if (label.equals("Reviewable")) {
        return "Reviewable"
      }
    }
    return "Node"
  }

  private def getReviewableById(id: Long, con: Connection): Reviewable = {
    val statement = con.prepareStatement("MATCH (r) where id(r) = {1} return r.name, r.description, r.thumbnail")
    statement.setLong(1, id)
    val rs = statement.executeQuery
    rs.next
    val name = rs.getString("r.name")
    val description = rs.getString("r.description")
    val thumbnail = rs.getString("r.thumbnail")
    val ratingCountStat = con.prepareStatement("MATCH (r)-[:RATING]->(rating:Rating) WHERE id(r)={1} return count(rating)")
    ratingCountStat.setLong(1, id);
    val ratingCountRs = ratingCountStat.executeQuery
    ratingCountRs.next
    val ratingCount = ratingCountRs.getLong("count(rating)")
    val ratings = getRatingsForReviewable(id, con)
    val average = getAverageRating(ratings)
    val reviewCount = getReviewCount(ratings)
    val discriminators = getDiscriminatorsForReviewable(id, con)
    val relatedReviewables = getRelatedReviewablesForReviewable(id, con)
    val labels = getLabelsForNode(id, con)
    rs.close
    statement.close
    new Reviewable(id, name, description, discriminators, average, ratingCount, reviewCount, ratings, thumbnail, relatedReviewables, labels)
  }

  def getRelatedReviewable(reviewableId: Long, relationship: String, toOrFrom: String): RelatedReviewable = {
    val con = ds.getConnection
    val relatedReviewable = getRelatedReviewable(reviewableId, relationship, toOrFrom, con)
    con.close
    return relatedReviewable
  }

  def getRelatedReviewable(id: Long, relationship: String, toOrFrom: String, con: Connection): RelatedReviewable = {
    val statement = con.prepareStatement("MATCH (r) where id(r) = {1} return r.name, r.description, r.thumbnail")
    statement.setLong(1, id)
    val rs = statement.executeQuery
    rs.next
    val name = rs.getString("r.name")
    val description = rs.getString("r.description")
    val thumbnail = rs.getString("r.thumbnail")
    val ratings = getRatingsForReviewable(id, con)
    val average = getAverageRating(ratings)
    rs.close
    statement.close
    val reviewable = new Reviewable(id = id, name = name, rating = average, description = description, thumbnail = thumbnail)
    new RelatedReviewable(reviewable, relationship, toOrFrom)

  }

  private def getReviewForRating(ratingId: Long, con: Connection): Review = {
    val statement = con.prepareStatement("MATCH (rating:Rating)<-[:REVIEW_FOR]-(review:Review)<-[:CREATED]-(user:User) WHERE id(rating)={1} RETURN id(review), review.review, id(user), user.name, user.email, user.password")
    statement.setLong(1, ratingId)
    val rs = statement.executeQuery
    if (rs.next) {
      val user = new User(id = rs.getLong("id(user)"), name = rs.getString("user.name"), email = rs.getString("user.email"), password = rs.getString("user.password"))
      val comments = getCommentsForReview(rs.getLong("id(review)"), con)
      statement.close
      return new Review(rs.getLong("id(review)"), rs.getString("review.review"), comments)
    }
    rs.close
    statement.close
    return null
  }

  def getRatingsForReviewable(reviewableId: Long): MutableList[Rating] = {
    val con = ds.getConnection
    val ratings = getRatingsForReviewable(reviewableId)
    con.close
    return ratings
  }

  private def getRatingsForReviewable(reviewableId: Long, con: Connection): MutableList[Rating] = {
    val statement = con.prepareStatement("MATCH (r)-[:RATING]->(rating:Rating) WHERE id(r)={1} return rating.rating, id(rating)")
    statement.setLong(1, reviewableId)
    val rs = statement.executeQuery
    var ratings = new MutableList[Rating]
    while (rs.next) {
      val rating = rs.getString("rating.rating").toDouble
      val ratingId = rs.getLong("id(rating)")
      val user = getCreatorForNode(ratingId, con)
      val review = getReviewForRating(ratingId, con)
      ratings += new Rating(id=ratingId, rating, review, user)
    }
    rs.close
    statement.close
    return ratings
  }
  
  private def getCommentsForReview(reviewId: Long, con: Connection): MutableList[Comment] = {
    val statement = con.prepareStatement("MATCH (review:Review)<-[:COMMENT_FOR]-(comment:Comment)<-[:CREATED]-(user:User) WHERE id(review)={1} RETURN comment.comment, comment.createddate, id(user), user.name, user.password, user.email")
    statement.setLong(1, reviewId)
    val rs = statement.executeQuery
    var comments = new MutableList[Comment]
    while (rs.next) {
      val user = new User(id=rs.getLong("id(user)"), name=rs.getString("user.name"), email=rs.getString("user.email"), password=rs.getString("user.password"))
      val comment = new Comment(user, rs.getString("comment.comment"), rs.getString("comment.createddate"))
      comments += comment
    }
    rs.close
    statement.close
    return comments
  }

  def getCommentsForReview(reviewId: Long): MutableList[Comment] = {
    val con = ds.getConnection
    val comments = getCommentsForReview(reviewId)
    con.close
    return comments
  }

  def getCreatorForNode(nodeId: Long): User = {
    val con = ds.getConnection
    val user = getCreatorForNode(nodeId, con)
    con.close
    return user
  }

  private def getCreatorForNode(nodeId: Long, con: Connection): User = {
    val statement = con.prepareStatement("MATCH (n)<-[:CREATED]-(u:User) WHERE id(n)={1} return id(u), u.name, u.password, u.email")
    statement.setLong(1, nodeId)
    val rs = statement.executeQuery
    rs.next
    rs.close
    statement.close
    return new User(id = rs.getLong("id(u)"), name = rs.getString("u.name"), email = rs.getString("u.email"), password = rs.getString("u.password"))
  }

  def getReviewCount(ratings: MutableList[Rating]): Long = {
    var count = 0
    for (rating <- ratings) {
      if (rating.review != null) {
        count += 1
      }
    }
    return count
  }

  def getDiscriminatorsForReviewable(id: Long): MutableList[Discriminator] = {
    val con = ds.getConnection
    val discriminators = getDiscriminatorsForReviewable(id, con)
    con.close
    return discriminators
  }
  private def getDiscriminatorsForReviewable(id: Long, con: Connection): MutableList[Discriminator] = {
    val statement = con.prepareStatement("MATCH (d:Discriminator)-[]->(r) WHERE id(r)={1} RETURN id(d), d.name, d.description");
    statement.setLong(1, id)
    val rs = statement.executeQuery
    var discriminators = new MutableList[Discriminator]
    while (rs.next) {
      val id = rs.getLong("id(d)")
      val name = rs.getString("d.name")
      val desc = rs.getString("d.description")
      val average = getAverageRating(getRatingsForReviewable(id, con))
      discriminators += new Discriminator(id = id, name = name, description = desc, rating = average)
    }
    rs.close
    statement.close
    return discriminators;
  }

  def getUserByName(name: String): User = {
    val con = ds.getConnection
    val user = getUserByName(name, con)
    con.close
    return user
  }

  private def getUserByName(name: String, con: Connection): User = {
    val statement = con.prepareStatement("MATCH (u:User) WHERE u.name={1} RETURN id(u), u.name, u.email, u.password")

    statement.setString(1, name)
    val rs = statement.executeQuery

    if (rs.next) {
      val usr = new User(rs.getLong("id(u)"), rs.getString("u.name"), rs.getString("u.email"), rs.getString("u.password"))
      rs.close
      statement.close
      usr
    } else
      throw new SQLException("Invalid user name: " + name)
  }

  def getRelatedReviewablesForReviewable(reviewableId: Long): MutableList[RelatedReviewable] = {
    val con = ds.getConnection
    val relatedReviewables = getRelatedReviewablesForReviewable(reviewableId, con)
    con.close
    return relatedReviewables
  }

  def getRelatedReviewablesForReviewable(reviewableId: Long, con: Connection): MutableList[RelatedReviewable] = {
    val statement = con.prepareStatement("MATCH (r:Reviewable)-[rel]->(n:Reviewable) WHERE id(r) = {1} return id(n), type(rel)")
    statement.setLong(1, reviewableId);
    val rs = statement.executeQuery();
    var relatedReviewables = new MutableList[RelatedReviewable]
    while (rs.next) {
      val id = rs.getLong("id(n)")
      val rel = rs.getString("type(rel)")
      val relatedReviewable = getRelatedReviewable(id, rel, "FROM", con)
      relatedReviewables += relatedReviewable
    }
    rs.close
    statement.close
    val statement2 = con.prepareStatement("MATCH (r:Reviewable)<-[rel]-(n:Reviewable) WHERE id(r) = {1} return id(n), type(rel)")
    statement2.setLong(1, reviewableId)
    val rs2 = statement2.executeQuery()
    while (rs2.next) {
      val id = rs2.getLong("id(n)")
      val rel = rs2.getString("type(rel)")
      val relatedReviewable = getRelatedReviewable(id, rel, "TO", con)
      relatedReviewables += relatedReviewable
    }
    rs2.close
    statement2.close
    return relatedReviewables
  }

  def searchReviewablesByName(name: String): MutableList[Reviewable] = {
    val con = ds.getConnection
    val reviewables = searchReviewablesByName(name, con)
    con.close
    return reviewables
  }
  private def searchReviewablesByName(name: String, con: Connection): MutableList[Reviewable] = {
    var reviewables = new MutableList[Reviewable]
    if (name == null || name == "") {
      val statement = con.prepareStatement("MATCH (r:Reviewable) WHERE r.name =~ ('(?i).*' + {1} + '.*') return id(r)")
      statement.setString(1, "[a-zA-Z]")
      val rs = statement.executeQuery
      while (rs.next) {
        val id = rs.getLong("id(r)")
        val rev = getReviewableById(id, con)
        reviewables += rev
      }
      rs.close
      statement.close
      return reviewables
    }

    val statement = con.prepareStatement("MATCH (r:Reviewable) WHERE r.name =~ ('(?i).*' + {1} + '.*') return id(r)")
    statement.setString(1, name)
    val rs = statement.executeQuery
    while (rs.next) {
      val id = rs.getLong("id(r)")
      val rev = getReviewableById(id, con)
      reviewables += rev
    }
    rs.close
    statement.close
    return reviewables
  }

  def getLablesForNode(nodeId: Long): MutableList[String] = {
    val con = ds.getConnection
    val labels = getLabelsForNode(nodeId, con)
    con.close
    return labels
  }

  private def getLabelsForNode(nodeId: Long, con: Connection): MutableList[String] = {
    val statement = con.prepareStatement("MATCH (r) WHERE id(r) = {1}  WITH DISTINCT labels(r) as labels UNWIND labels as label RETURN DISTINCT label ORDER BY label")
    statement.setLong(1, nodeId)
    val rs = statement.executeQuery
    val labels = new MutableList[String]
    while (rs.next) {
      labels += (rs.getString("label"))
    }
    rs.close
    statement.close
    return labels
  }

  def addUser(user: User) {
    val con = ds.getConnection
    val statement = con.prepareStatement("CREATE (usr:User {name:{1}, email:{2}, password:{3}, status:{4}}) return id(usr)")

    statement.setString(1, user.name)
    statement.setString(2, user.email)
    statement.setString(3, user.password)
    statement.setInt(4, user.status)
    val rs = statement.executeQuery();
    if (rs.next()) {
      setNodeID(rs.getLong(1), con)
    } else {
      throw new SQLException("Creating User failed, no ID obtained.");
    }
    rs.close
    statement.close
    con.close
  }

  def addReviewable(rev: Reviewable) {
    val con = ds.getConnection
    val statement = con.prepareStatement("CREATE (rev:Reviewable {name:{1}, description:{2},thumbnail:{3}, status:{4} }) return id(rev)", Statement.RETURN_GENERATED_KEYS)

    statement.setString(1, rev.name)
    statement.setString(2, rev.description)
    statement.setString(3, rev.thumbnail)
    statement.setInt(4, rev.status)

    val rs = statement.executeQuery();
    if (rs.next()) {
      setNodeID(rs.getLong(1), con)
    } else {
      throw new SQLException("Creating Reviewable failed, no ID obtained.");
    }
    rs.close
    statement.close
    con.close
  }

  def addDiscriminator(disc: Discriminator, nodeId : Long) {

    val con = ds.getConnection
    val statement = con.prepareStatement("MATCH (n) where id(n) = {1} CREATE (:Discriminator {name:{2}, description:{3}, status:{4} } )-[:DISCRIMINATES]->(n)")
    statement.setLong(1, nodeId)
    statement.setString(2, disc.name)
    statement.setString(3, disc.description)
    statement.setInt(4, disc.status)
    statement.executeQuery();
    statement.close
    con.close()
  }

  def addRatingForNode(user: User, nodeId: Long, rating: Rating) = {
    val con = ds.getConnection
    println("rating.review.review : " + rating.review.review)
    val checkForRating = con.prepareStatement("MATCH (n)-[:RATING]->(r:Rating)<-[:CREATED]-(u) WHERE id(n) = {1} and id(u)={2} return id(r)")
    checkForRating.setLong(1, nodeId)
    checkForRating.setLong(2, user.id)
    val existingIdRs = checkForRating.executeQuery
    if(!existingIdRs.next){
      println("Existing id does not exist.  Creating rating.")
    val statement = con.prepareStatement("MATCH (n), (u) where id(n) = {1} and id(u) = {2} CREATE (n)-[:RATING]->(rating:Rating {rating:{3}})<-[:CREATED]-(u) return id(rating)")
    statement.setLong(1, nodeId)
    statement.setLong(2, user.id)
    statement.setDouble(3, rating.rating)
    val rs = statement.executeQuery
    rs.next
    val ratingId = rs.getLong("id(rating)")
    if (rating.review == null || rating.review.review == null || rating.review.review == "") { /// :-(
    } else {
      println("Creating review")
      println("ratingId: " + ratingId)
      println("userId: " + user.id)
      println("review: " + rating.review.review)
      val createRating = con.prepareStatement("MATCH (n), (u) where id(n) = {1} and id(u) = {2} CREATE (n)<-[:REVIEW_FOR]-(review:Review {review:{3}})<-[:CREATED]-(u) return id(review)")
      createRating.setLong(1, ratingId)
      createRating.setLong(2, user.id)
      createRating.setString(3, rating.review.review)
      createRating.executeQuery
    }
    rs.close
    statement.close
    } else {
      val existingId = existingIdRs.getLong("id(r)")
      val statement = con.prepareStatement("MATCH (r) WHERE id(r) = {1} SET r.rating = {2}")
      println("updating rating.")
      println("rating id: " + rating.id)
      println("rating: " + rating.rating)
      statement.setLong(1, existingId)
      statement.setDouble(2, rating.rating)
      statement.executeQuery
      statement.close
      println("rating updated.")
      val review = getReviewForRating(existingId)
      if (rating.review.review != null) {
      val checkForReview = con.prepareStatement("MATCH (r:Rating)<-[:REVIEW_FOR]-(review) where id(r) = {1} return id(review)")
      checkForReview.setLong(1, existingId)
      val reviewRs = checkForReview.executeQuery
      if (reviewRs.next){
        val existingReview = reviewRs.getLong("id(review)")
        println("updating review")
        val updateReview = con.prepareStatement("MATCH (r:Review) WHERE id(r) = {1} SET r.review = {2}")
        updateReview.setLong(1, existingReview)
        updateReview.setString(2, rating.review.review)
        updateReview.executeQuery
        updateReview.close
      }
      else {
        println("adding review")
        val addReview = con.prepareStatement("MATCH (r) WHERE id(r) = {1} CREATE (r)<-[:REVIEW_FOR]-(n:Review {review : {2}})")
        addReview.setLong(1, existingId)
        addReview.setString(2, rating.review.review)
        addReview.executeQuery
        addReview.close
      }
      }
      
    }
    con.close
  }

  def addRatingForNode(node: Node, rating: Rating) = {

    val user = rating.user
    val con = ds.getConnection
    val stm1 = con.prepareStatement("MATCH (n) where id(n) = {1} CREATE (r:Rating {userId:{2}, rating:{3}, status:{4} } )-[:RATES]->(n) return id(r) ")

    var sql = ""

    if (node.isInstanceOf[Discriminator])
      sql = "MATCH (a:Discriminator),(b:User) WHERE a.id = {1} AND b.id = {2} CREATE (a)-[r:RATED_BY]->(b)"
    else
      sql = "MATCH (a:Reviewable)   ,(b:User) WHERE a.id = {1} AND b.id = {2} CREATE (a)-[r:RATED_BY]->(b)"

    val stm2 = con.prepareStatement(sql)

    val stm3 = con.prepareStatement("MATCH (a:Rating),(b:User) WHERE a.id = {1} AND b.id = {2} CREATE (a)-[r:CREATED_BY]->(b)")

    stm1.setLong(1, node.id)
    stm1.setLong(2, user.id)
    stm1.setDouble(3, rating.rating)
    stm1.setInt(4, rating.status)

    val rs = stm1.executeQuery

    if (rs.next) {
      setNodeID(rs.getLong(1), con)
    } else {
      throw new SQLException("Creating Rating for node  failed, no ID obtained.");
    }

    stm2.setLong(1, node.id)
    stm2.setLong(2, user.id)
    stm2.executeQuery
    // Create relationship between User and Rating
    stm3.setLong(1, rs.getLong(1)) // new rating Id 
    stm3.setLong(2, user.id)
    stm3.executeQuery

    rs.close
    stm1.close
    stm2.close
    stm3.close
    con.close
  }

  def addCommentForReview(review: Review, comment: Comment) {

    val con = ds.getConnection
    val stm = con.prepareStatement("MATCH (n) where id(n) = {1} CREATE (c:Comment {text:{3} } )-[:COMMENTS_ON]->(n) return ")

    stm.setLong(1, review.id)
    stm.setString(2, comment.comment)
    stm.executeQuery
    stm.close
    con.close
  }

  def setNodeID(id: Long, conn: Connection) {
    val stm = conn.prepareStatement("MATCH (n) where id (n) = {1} SET n.id = id(n)")
    stm.setLong(1, id)
    stm.executeQuery()
    stm.close()
  }
}