package persistence

import model._
import scala.collection.mutable.MutableList
import play.api.db._

abstract class KingFisherDAO {

  def getReviewableById(id: Long): Reviewable

  def getAverageRating(ratings: MutableList[Rating]): Double
  
  def getDiscriminatorById(id: Long) : Discriminator

  def getReviewForRating(ratingId: Long): Review

  def getRatingsForReviewable(reviewableId: Long): MutableList[Rating]

  def getCommentsForReview(reviewId: Long): MutableList[Comment]

  def getCreatorForNode(nodeId: Long): User

  def getReviewCount(ratings: MutableList[Rating]): Long

  def getDiscriminatorsForReviewable(id: Long): MutableList[Discriminator]
  def getUserByName(name: String): User
  
  //def getRatingsForDiscriminator() : List[Rating]

  //def addCommentForReview(reviewId: Long, comment: Comment)

  def addUser(user: User)
  def addReviewable(rev: Reviewable)
  def addDiscriminator(disc: Discriminator,nodeId:Long)
  def addRatingForNode(node:Node, rating: Rating)
  
  def addRatingForNode(user : User, nodeId : Long, rating : Rating)
  
  def addCommentForReview(reviewId : Long, comment : Comment, user : User)
  
  //def updateRatingForNode(user : User, nodeId : Long, rating : Rating, existingRatingId : Long) 

 //  def addCommentForReview(comment: Comment, reviewId: Long)

  def searchReviewablesByName(name : String) : MutableList[Reviewable] 
  
  def getLabelsForNode(id: Long): MutableList[String] 
}